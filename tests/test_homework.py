import pytest
from selenium.webdriver import Chrome
from utils.random_message import generate_random_text
from pages.arena_home import HomePageArena
from pages.arena_login import ArenaLoginPage
from pages.arena_project import ArenaProjectPage
from pages.arena_project import ArenaProjectDetails
from pages.arena_project import ProjectProperties
from pages.arena_project import SearchResultPage



@pytest.fixture()
def browser():
    driver = Chrome()
    driver.get('http://demo.testarena.pl/zaloguj')
    arena_login_page = ArenaLoginPage(driver)
    arena_login_page.login("administrator@testarena.pl", "sumXQQ72$L")
    arena_home_page = HomePageArena(driver)
    arena_home_page.click_tools_icon()
    yield driver
    driver.quit()


def test_verify_creating_new_project(browser):
    arena_project_page = ArenaProjectPage(browser)
    arena_project_page.click_dodaj_projekt()
    arena_project_details = ArenaProjectDetails(browser)
    name_of_the_project = generate_random_text(5) + "Adrian"
    arena_project_details.add_generated_data_to_new_project(name_of_the_project)
    arena_project_details.click_zapisz()
    project_properties = ProjectProperties(browser)
    project_properties.click_projekty()
    arena_project_page.search_for_created_project(name_of_the_project)
    search_result_page = SearchResultPage(browser)
    search_result_page.verify_if_project_created_correctly(name_of_the_project)

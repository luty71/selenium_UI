from selenium.webdriver import Keys
from selenium.webdriver.common.by import By
from utils.random_message import generate_random_text



class ArenaProjectPage:

    def __init__(self, browser):
        self.browser = browser

    def click_dodaj_projekt(self):
        self.browser.find_element(By.CSS_SELECTOR, "a[href='http://demo.testarena.pl/administration/add_project']").click()

    def search_for_created_project(self, name_of_the_project):
        self.browser.find_element(By.CSS_SELECTOR, "#search").send_keys(name_of_the_project + Keys.ENTER)

class ArenaProjectDetails:

    def __init__(self, browser):
        self.browser = browser

    def add_generated_data_to_new_project(self, name_of_the_project):
        nazwa = self.browser.find_element(By.ID, 'name')
        nazwa.send_keys(name_of_the_project)
        self.browser.find_element(By.ID, 'prefix').send_keys(generate_random_text(2) + "Luty")
        self.browser.find_element(By.ID, 'description').send_keys(generate_random_text(40) + "UI test")

    def click_zapisz(self):
        self.browser.find_element(By.CSS_SELECTOR, "input#save").click()

class ProjectProperties:

    def __init__(self, browser):
        self.browser = browser

    def click_projekty(self):
        self.browser.find_element(By.CSS_SELECTOR, "a[href='http://demo.testarena.pl/administration/projects']").click()

class SearchResultPage:
    def __init__(self, browser):
        self.browser = browser

    def verify_if_project_created_correctly(self, name_of_the_project):
        assert name_of_the_project in self.browser.find_element(By.CSS_SELECTOR, "td").text
